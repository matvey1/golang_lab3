package main

import (
	"fmt"
	"github.com/fogleman/gg"
	"github.com/paulmach/go.geojson"
	"math/rand"
	"os"
	"time"
)




func read_file() []byte{
	file, err := os.Open("map.geojson")
	if err != nil {
		// здесь перехватывается ошибка
		print("error")
	}
	defer file.Close()

	// получить размер файла
	stat, err := file.Stat()
	// чтение файла
	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)

	return bs
}


func main() {


	fc1,err := geojson.UnmarshalFeatureCollection(read_file())
	fmt.Println(fc1.Features[0].Geometry.Polygon[0])


	var array_of_dots [13][][] float64
	for i := 0; i < len(fc1.Features); i++ {
		array_of_dots[i] = fc1.Features[i].Geometry.Polygon[0]
	}
	fmt.Println(err)


	var max_x,max_y,min_x,min_y float64
	max_x = -3000
	max_y = -3000
	min_x = 3000
	min_y = 3000

	for j := 0; j < len(array_of_dots); j++ {
		for i := 0; i < len(array_of_dots[j]); i++ {
			if (array_of_dots[j][i][0] > max_x) {
				max_x = array_of_dots[j][i][0]
			}
			if (array_of_dots[j][i][0] < min_x) {
				min_x = array_of_dots[j][i][0]
			}
		}

		for i := 0; i < len(array_of_dots[j]); i++ {
			if (array_of_dots[j][i][1] > max_y) {
				max_y = array_of_dots[j][i][1]
			}
			if (array_of_dots[j][i][1] < min_y) {
				min_y = array_of_dots[j][i][1]
			}
		}
	}
	println("max x")
	println(int(max_x))
	println("max y")
	println(int(max_y))
	println("min x")
	println(int(min_x))
	println("min y")
	println(int(min_y))


	window_height := 1024
	window_width := 1366
	dc := gg.NewContext(window_width, window_height)
	rand.Seed(time.Now().UnixNano())

	for j := 0; j < len(array_of_dots); j++ {
		dc.SetRGB(float64(rand.Intn(255)), float64(rand.Intn(255)), float64(rand.Intn(255)))
		for i := 0; i < len(array_of_dots[j]); i++ {
			x := (array_of_dots[j][i][0] - min_x) * (max_x - min_x) / 6
			y := float64(window_height)/1.1 - (array_of_dots[j][i][1]-min_y)*(max_y-min_y)/6
			dc.LineTo(x, y)
		}
		dc.SetFillRule(gg.FillRuleEvenOdd)
		dc.FillPreserve()
		dc.SetRGBA(255,255, 255,255)
		dc.SetLineWidth(6)
		dc.Stroke()
		dc.Fill()
	}
	dc.SavePNG("out.png")

}
